const ArrayMonad = xs => {
  const map = fn => ArrayMonad(xs.map(x => fn(x)))
  const chain = fn => ArrayMonad(xs.reduce((ys, x) => [...ys, ...fn(x).xs], []))
  const ap = mys => chain(f => mys['fantasy-land/map'](y => f(y)));
  return {
    xs,
    'fantasy-land/map': map,
    'fantasy-land/chain': chain,
    'fantasy-land/ap': ap,
    'constructor': ArrayMonad
  };
};

ArrayMonad['fantasy-land/of'] = x => ArrayMonad([x]);

module.exports = ArrayMonad;